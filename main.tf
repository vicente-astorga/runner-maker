# Variables
variable "google_credentials_path" {
  description = "Path to the Google Cloud credentials JSON file"
  type        = string
  default     = "/creds/serviceaccount.json"
}

# Provider config
provider "google" {
  credentials = file(var.google_credentials_path)
  project     = "devops-387014"
  region      = "us-west4"
}

# VM
resource "random_id" "instance_id" {
  byte_length = 4
}

resource "google_compute_instance" "vm-gitlab-runner" {
  name         = "vm-gitlab-runner-${random_id.instance_id.hex}"
  machine_type = "n1-standard-1"
  zone         = "us-west4-b"

  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7"
    }
  }

  network_interface {
    network = "default"

    access_config {}
  }

  tags = ["gitlab-runner", "ansible"]

  provisioner "local-exec" {
    command = "echo 'instance_ip = ${google_compute_instance.vm-gitlab-runner.network_interface.0.access_config.0.nat_ip}' > instance_ip.txt"
  }
}





